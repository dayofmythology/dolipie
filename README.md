# DoliPie

Application built using the Django framework to extend the functionality of Dolibarr CRM/ERP

# Features
Additional Reporting functionality
Mobile Friendly Interface to track/fulfill orders, Customer Signature on delivery etc.